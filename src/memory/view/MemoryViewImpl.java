package memory.view;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.animation.AnimationTimer;
import javafx.animation.RotateTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import memory.MemoryContrac;
import memory.MemoryContrac.MemoryPresenter;
import memory.MemoryContrac.MemoryView;
import memory.model.Tema;


public class MemoryViewImpl implements Initializable, MemoryView {
   
    @FXML
    private VBox vBox;
        
    @FXML
    private MemoryPresenter presenter;

    @FXML
    GridPane gridPaneBoard; 
    
    @FXML
    MenuItem intents15, intents20, intents25;
    
    @FXML
    MenuItem defaultTema, hearthstoneTema;
    
    Scene scene;
    Stage stage;
    
    ImageView imgVaux1, imgVaux2;
    int mov = 0;
    List<Image> imagenes;
    
    private double opacidad = 1;
    
    Tema tema = Tema.DEFAULT;
    int numAttempts = 15;

    public MemoryViewImpl(Stage stage, MemoryPresenter presenter) {
        this.presenter = presenter;
        initUI(stage);
    }

    private void initUI(Stage stage) {
        try {
            
            FXMLLoader loader = new FXMLLoader();
            loader.setController(this);
            loader.setLocation(getClass().getResource("FXMLMemory.fxml"));
            Parent root = loader.load();
            this.scene = new Scene(root);
            this.stage = stage;
            
            intents15.setOnAction((ActionEvent t) -> {
                numAttempts = 15;
                iniciar();
            });
            
            intents20.setOnAction((ActionEvent t) -> {
                numAttempts = 20;
                iniciar();
            });
            
            intents25.setOnAction((ActionEvent t) -> {
                numAttempts = 25;
                iniciar();
            });
            
            defaultTema.setOnAction((ActionEvent t) -> {
                tema = Tema.DEFAULT;
                iniciar();
            });
            
            hearthstoneTema.setOnAction((ActionEvent t) -> {
                tema = Tema.HEARTHSTONE;
                iniciar();
            });
            
            iniciar();
            
            //stage.setResizable(false);
            stage.setScene(scene);
            stage.show();
          
        } catch (IOException e) {
            System.out.println("ERROR 1\n:");
            e.printStackTrace();
        }
        
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public void setPresenter(MemoryContrac.MemoryPresenter p) {
        this.presenter = p;
    }

    @Override
    public void showSecondCard(boolean match, int remainingAttempts, Image image) {
        
        imgVaux2.setImage(image);
        
        if(!match){
            /*Deshabilitamos el gridPane para evitar clicks*/
            gridPaneBoard.setDisable(true);
            
            /*Iniciamos animacion fade-out*/
            AnimationTimer timer = new MyTimer();
            timer.start();
            
            /*Iniciamos rotaciones de las cartas*/
            RotateTransition rt = new RotateTransition(Duration.millis(500), imgVaux2);
            rt.setByAngle(360);
            rt.play();

            RotateTransition rt2 = new RotateTransition(Duration.millis(500), imgVaux1);
            rt2.setByAngle(360);
            rt2.play();
        }else{
            imgVaux1.setDisable(true);
            imgVaux2.setDisable(true);
        }
    } 
    
    private class MyTimer extends AnimationTimer {

      
        @Override
        public void handle(long now) {

            doHandle();
        }

        private void doHandle() {

            opacidad -= 0.02;
            
            /*Actualizamos la opacidad*/
            imgVaux1.opacityProperty().set(opacidad);
            imgVaux2.opacityProperty().set(opacidad);
            
            if (opacidad <= 0) {
                /*Añadimos las imagenes*/
                imgVaux1.setImage(imagenes.get(1));
                imgVaux2.setImage(imagenes.get(1));
                
                /*Reiniciamos la opacidad*/
                opacidad=1;
                
                /*Las hacemos visibles de nuevo*/
                imgVaux1.opacityProperty().set(opacidad);
                imgVaux2.opacityProperty().set(opacidad);
                
                /*Volvemos a habilitar el gridpane*/
                gridPaneBoard.setDisable(false);
                stop();
            }
        }
    }

    @Override
    public void gameOver(Image image) {
        
        imgVaux2.setImage(image);
        gridPaneBoard.setDisable(true);
        
        Alert helpAlert = new Alert(Alert.AlertType.INFORMATION, "HAS PEDIDO!", ButtonType.CLOSE);
        helpAlert.showAndWait();
       
    }

    @Override
    public void gameWin(Image image) {
        
        imgVaux2.setImage(image);
        gridPaneBoard.setDisable(true);

        Alert helpAlert = new Alert(Alert.AlertType.INFORMATION, "HAS GANADO!", ButtonType.CLOSE);
        helpAlert.showAndWait();
    }

    @Override
    public void showFirstCard(Image image) {
        imgVaux1.setImage(image);
    }

    class MyImageView extends ImageView {

        private int pos;

        public MyImageView(int pos, Image image) {
            super(image);
            this.pos = pos;
        }

        public int getPos() {
            return pos;
        }

        public void setPos(int pos) {
            this.pos = pos;
        }

    }

    @FXML
    public void ExitGame(){
        Platform.exit();
    }
    
    @FXML
    public void helpContents(){
        Alert helpAlert = new Alert(Alert.AlertType.INFORMATION, "Deberas elejir mediante un click la primera carta,"
                + " esta se destapara mostrando su numero, y tu deber sera encontrar su pareja.\n "
                + "Note olvide de sus posiciones no las encontraras a la primera. Suerte!", ButtonType.CLOSE);
        helpAlert.setTitle("Help Content");
        helpAlert.setHeaderText("Como jugar.");
        helpAlert.showAndWait();
    }
    
    @FXML
    public void aboutInfo(){
        Alert helpAlert = new Alert(Alert.AlertType.INFORMATION, "El \"autor\" de este juego es David Anaya", ButtonType.CLOSE);
        helpAlert.setTitle("About info");
        helpAlert.showAndWait();
    }
    
    
    public GridPane crearTablero(Image imagen) {

        GridPane grid = new GridPane();
        int count = 0;
        
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                MyImageView image = new MyImageView(count, imagen);
                /*fijamos la altura y anchura de imageView*/
                image.setFitHeight(150);
                image.setFitWidth(90);
                image.setId(count+"");
                
                image.setOnMouseClicked((MouseEvent event) -> {
                    if(this.mov == 0){
                        this.mov = 1; 
                        imgVaux1 = (MyImageView) event.getSource();
                        showFirstCard(presenter.toPlayFirstCard(Integer.parseInt( ((MyImageView) event.getSource()).getId() )));
                    }else{
                        this.mov = 0;
                        imgVaux2 = (MyImageView) event.getSource();
                        presenter.toPlaySecondCard(Integer.parseInt( ((MyImageView) event.getSource()).getId() ));
                    }
                });
                /*Posicion en la tabla*/
                grid.add(image, j+1, i);
                count++;
            }
        }
        
        grid.setVgap(20);
        grid.setHgap(20);
        grid.setPadding(new Insets(150, (vBox.getMinWidth()/2 - grid.getMinWidth()/2), 150, (vBox.getMinWidth()/2 - grid.getMinWidth()/2 )));
        
        return grid;
    }
    
    public void setBackgroundGame(Image imagen){
        vBox.setBackground(new Background(new BackgroundImage(
                imagen,
                BackgroundRepeat.ROUND,
                BackgroundRepeat.ROUND,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT))
        );
        
        vBox.setMinWidth(imagen.getWidth());
        vBox.setMinHeight(imagen.getHeight());
    }
    
    public void iniciar(){
        
        cleanAll();
       
        presenter.toStart(tema, numAttempts);
        imagenes = presenter.toInitImages();

        setBackgroundGame(imagenes.get(0));
        
        gridPaneBoard = crearTablero(imagenes.get(1));
        gridPaneBoard.setDisable(false);
        
        vBox.getChildren().addAll(gridPaneBoard);
        
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }
    
    public void cleanAll(){
        vBox.getChildren().remove(gridPaneBoard);
        imagenes = null;
    }
    
}
