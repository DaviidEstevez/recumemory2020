package memory.controller;

import java.util.List;
import javafx.scene.image.Image;
import memory.MemoryContrac.MemoryModel;
import memory.MemoryContrac.MemoryPresenter;
import memory.MemoryContrac.MemoryView;
import memory.model.GameStatutsChangedEvent;
import memory.model.MemoryModelImpl;
import memory.model.Tema;

public class MemoryPresenterImpl implements MemoryPresenter {

    private MemoryModel m;
    private MemoryView v;

    @Override
    public void setModel(MemoryModel m) {
        this.m = m;
    }
    
    @Override
    public void setView(MemoryView v) {
        this.v = v;
    }
    
    @Override
    public void toStart(Tema tema, int numAttempts) {
        m.removeListener(this);
        m.start(tema, numAttempts);
        m.addListener(this);
    }
    
    @Override
    public List<Image> toInitImages() {
       return m.getInitImages();
    }
    
    @Override
    public int toNumAttempts() {
        return m.getNumAttempts();
    }
    
    @Override
    public Image toPlayFirstCard(int pos) {
        return m.playFirstCard(pos);
    }
    
    @Override
    public void toPlaySecondCard(int pos) {
        m.playSecondCard(pos);
    }
    
    @Override
    public void isMatch(GameStatutsChangedEvent e, boolean macth) {        
        MemoryModelImpl modelo = (MemoryModelImpl) e.getSource();
        v.showSecondCard(macth, modelo.getNumAttempts(), modelo.getSecondImageCard());  
    }
    
    @Override
    public void over(GameStatutsChangedEvent e) {
        MemoryModelImpl modelo = (MemoryModelImpl) e.getSource();
        v.gameOver(modelo.getSecondImageCard());
        
    }
    
    @Override
    public void win(GameStatutsChangedEvent e) {        
        MemoryModelImpl modelo = (MemoryModelImpl) e.getSource();
        v.gameWin(modelo.getSecondImageCard());
        
    }
}