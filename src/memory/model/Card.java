package memory.model;

import javafx.scene.image.Image;


public class Card {
    
    private Image image;
    private boolean isFound;

    public Card(Image image, boolean isFound) {
        this.image = image;
        this.isFound = isFound;
    }

    public Image getImage() {
        return image;
    }

    public boolean isIsFound() {
        return isFound;
    }

    public void setIsFound(boolean isFound) {
        this.isFound = isFound;
    }
}

