package memory.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.scene.image.Image;
import memory.MemoryContrac.MemoryModel;
import memory.MemoryContrac.MemoryModelListener;

public class MemoryModelImpl implements MemoryModel {
    
    Tema tema;
    int numAttempts;
    int posicion1 = -1;
    
    ArrayList<Card> cartas;
    ArrayList<MemoryModelListener> listeners = new ArrayList<MemoryModelListener>();
    
    Image imgAux;
    
    @Override
    public boolean addListener(MemoryModelListener listener) {
        return listeners.add(listener);
    }

    @Override
    public boolean removeListener(MemoryModelListener listener) {
        return listeners.remove(listener);
    }

    @Override
    public void start(Tema tema, int numAttempts) {
        cartas = new ArrayList<Card>();
        this.tema = tema;
        this.numAttempts = numAttempts;
        String path = "file:.\\src\\memory\\resources\\Default\\";
        if(tema.equals(Tema.HEARTHSTONE)){
             path = "file:.\\src\\memory\\resources\\Hearthstone\\";
        }
        
        for(int i = 0; i < 8; i++){
            Card carta = new Card(new Image(path+(i+1)+".png"), false);
            cartas.add(carta);
            cartas.add(carta);
        }
        
        Collections.shuffle(cartas);
    }

    @Override
    public Image playFirstCard(int pos) {
        posicion1 = pos;
        return cartas.get(pos).getImage();
    }

    @Override
    public void playSecondCard(int pos) {
     
      imgAux = cartas.get(pos).getImage();
      int chivato = 0;
      
      for(MemoryModelListener listener : listeners) {
          
        if(cartas.get(pos).equals(cartas.get(posicion1))){
            /*llamamos al listener y cambiamos el estado de las cartas a encontrado*/
            listener.isMatch(new GameStatutsChangedEvent(this), true);
            cartas.get(pos).setIsFound(true);
            cartas.get(posicion1).setIsFound(true);
            
            /*Comprobamos que no queden cartas por encontrar*/
            
            for (Card carta : cartas) {
                if(!carta.isIsFound()){
                    chivato = 1;
                    break;
                }
            }
            if(chivato != 1){
                listener.win(new GameStatutsChangedEvent(this));
            }
            
        }else{
            /*Restamos un intento y avisamos a listener de que no ha sido match*/
            
            
            if(numAttempts > 0){
                listener.isMatch(new GameStatutsChangedEvent(this), false);
            }else{
                listener.over(new GameStatutsChangedEvent(this));
            }
        }
      }
    }

    @Override
    public List<Image> getInitImages() {
        List<Image> imagenes = new ArrayList<Image>(); 
        if(tema.equals(Tema.DEFAULT)){
            imagenes.add(new Image("file:.\\src\\memory\\resources\\Default\\-1.jpg"));
            imagenes.add(new Image("file:.\\src\\memory\\resources\\Default\\0.png"));
        }else{
            imagenes.add(new Image("file:.\\src\\memory\\resources\\Hearthstone\\-1.jpg"));
            imagenes.add(new Image("file:.\\src\\memory\\resources\\Hearthstone\\0.png"));
        }
        return imagenes;
    }

    @Override
    public int getNumAttempts() {
        return numAttempts--;
    }

    @Override
    public Image getSecondImageCard() {
        return imgAux;
    }

}
