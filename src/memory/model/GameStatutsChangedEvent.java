package memory.model;

import java.util.EventObject;


public class GameStatutsChangedEvent extends EventObject{

    public GameStatutsChangedEvent(Object source) {
        super(source);
    }

}
