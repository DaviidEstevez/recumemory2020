package memory;

import java.util.List;
import javafx.scene.image.Image;
import memory.model.GameStatutsChangedEvent;
import memory.model.Tema;

public interface MemoryContrac {

    interface MemoryGame {

        // methods that can be called by presenter
        void start(Tema tema, int numAttempts);

        Image playFirstCard(int pos);

        void playSecondCard(int pos);

        List<Image> getInitImages();

        int getNumAttempts();

        Image getSecondImageCard();

    }

    //the model contract     
    interface MemoryModel extends MemoryGame {

        boolean addListener(MemoryModelListener listener);

        boolean removeListener(MemoryModelListener listener);
    }

    //the presenter contract     
    interface MemoryPresenter extends MemoryModelListener {

        void setModel(MemoryModel m);

        void setView(MemoryView v);

        void toStart(Tema tema, int numAttempts);

        List<Image> toInitImages();

        int toNumAttempts();

        Image toPlayFirstCard(int pos);

        void toPlaySecondCard(int pos);

    }

    //the view contract     
    interface MemoryView {

        void setPresenter(MemoryPresenter p);

        // methods called by presenter
        void showFirstCard(Image image);

        void showSecondCard(boolean match, int remainingAttempts, Image image);

        void gameOver(Image image);

        void gameWin(Image image);

    }

    interface MemoryModelListener {

        //when the game is over or the two cards are equals, the listeners are notified.
        //notificats pel Game
        void isMatch(GameStatutsChangedEvent e, boolean macth);

        void over(GameStatutsChangedEvent e);

        void win(GameStatutsChangedEvent e);

    }

}
