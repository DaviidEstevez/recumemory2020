
package memory;

import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;
import memory.MemoryContrac.MemoryModel;
import memory.MemoryContrac.MemoryPresenter;
import memory.MemoryContrac.MemoryView;
import memory.controller.MemoryPresenterImpl;
import memory.model.MemoryModelImpl;
import memory.view.MemoryViewImpl;


public class MemoryApp extends Application {
    
  @Override
    public void start(Stage stage) throws IOException {

        MemoryPresenter p;
        MemoryModel m;
        MemoryView v;

        p = new MemoryPresenterImpl();
        m = new MemoryModelImpl();
       

        p.setModel(m); 
        v = new MemoryViewImpl(stage,p);
        p.setView(v);       
    }

    public static void main(String[] args) {
        launch(args);
    }
    
}
